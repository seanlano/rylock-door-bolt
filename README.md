# Rylock Door Bolt

At home I have a 2000s vintage glass French door, made by Rylock, which has a sliding bolt mechanism in it. The mechanism partly broke, and no spares were available, so I made this 3D model to attempt printing a replacement. 

Printed in ABS plastic, the part should be reasonably strong. To complete it, an appropriate length 8 mm steel bolt is needed, with the head cut off it. ABS parts can be joined with acetone, so the separate printed sections can be joined around the steel bolt. 

I used FreeCAD 0.18 to create the model. 

![Rendering](render_1.png)

# Licence

All hardware models and design files are released under the CERN Open Hardware Licence v1.2. Please see [LICENCE](LICENCE) for details.

